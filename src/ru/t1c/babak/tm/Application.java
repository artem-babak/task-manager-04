package ru.t1c.babak.tm;

import ru.t1c.babak.tm.constant.TerminalConst;

public final class Application {

    public static void main(final String[] args) {
        run(args);
    }

    public static void run(final String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        if (arg == null) return;
        switch (arg) {
            case TerminalConst.ABOUT:
                showAbout();
                break;
            case TerminalConst.VERSION:
                showVersion();
                break;
            case TerminalConst.HELP:
                showHelp();
                break;
            default:
                showError(arg);
                break;
        }
    }

    public static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Name: Babak Artem");
        System.out.println("E-mail: ababak@t1-consulting.ru");
    }

    public static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.1.0");
    }

    public static void showHelp() {
        System.out.println("[HELP]");
        System.out.printf("%s - Show developer info.\n", TerminalConst.ABOUT);
        System.out.printf("%s - Show application version.\n", TerminalConst.VERSION);
        System.out.printf("%s - Show terminal commands.\n", TerminalConst.HELP);
    }

    public static void showError(String arg) {
        System.err.printf("Error! Unknown argument: `%s`", arg);
    }

}
